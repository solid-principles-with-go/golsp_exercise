// -- ****************************************************************/
// -- ****************************************************************/
// --                                                 ,,             */
// --          `7MMM.     ,MMF'         `7MMF'        db             */
// --            MMMb    dPMM             MM                         */
// --            M YM   ,M MM   .gP"Ya    MM        `7MM             */
// --            M  Mb  M' MM  ,M'   Yb   MM          MM             */
// --            M  YM.P'  MM  8M""""""   MM      ,   MM             */
// --            M  `YM'   MM  YM.    ,   MM     ,M   MM             */
// --          .JML. `'  .JMML. `Mbmmd' .JMMmmmmMMM .JMML.           */
// -- ****************************************************************/
// -- ****************************************************************/
/* -- ****************************************************************/
/* --                          droneZ.go                             */
/* --                                                                */
/* --  Descripcion: Estructura general de los Drones.                */
/* --                                                                */
/* --   @ Autor  : Rodrigo G. Higuera M. <rodrigoghm@gmail.com>      */
/* --                                                                */
/* --  © 2022 - Mercado Libre - Desafio Tecnico SOLID                */
/* -- ****************************************************************/

package droneZ

import "fmt"

func (z *DroneZ) Preparar() {
	z.CheckBateria()
	z.CheckHelices()
}

func (z *DroneZ) CheckBateria() {
	fmt.Println("[Preparando] Chequeo de estado de bateria ... ")
}

func (z *DroneZ) CheckHelices() {
	fmt.Println("[Preparando] Chequeo de Estado de las Helices ... ")
}

func (z *DroneZ) Despegar() {
	fmt.Println("[Despengando] Despegando Ahora... ")
}

func (z *DroneZ) HealthCheck() {
	fmt.Println("[Volando] En el aire, Todo OK, Balanceo Automatico Habilitado ... ")
}

func (z *DroneZ) PiruetaEnZ(str string) {
	fmt.Println(str)
}

func (z *DroneZ) SetNombre(n string) {
	z.DroneX.Nombre = n
}

func (z *DroneZ) SetModelo(m string) {
	z.DroneX.Modelo = m
}

func (z *DroneZ) GetNombre() string {
	return z.DroneX.Nombre
}

func (z *DroneZ) GetModelo() string {
	return z.DroneX.Modelo
}
