// -- ****************************************************************/
// -- ****************************************************************/
// --                                                 ,,             */
// --          `7MMM.     ,MMF'         `7MMF'        db             */
// --            MMMb    dPMM             MM                         */
// --            M YM   ,M MM   .gP"Ya    MM        `7MM             */
// --            M  Mb  M' MM  ,M'   Yb   MM          MM             */
// --            M  YM.P'  MM  8M""""""   MM      ,   MM             */
// --            M  `YM'   MM  YM.    ,   MM     ,M   MM             */
// --          .JML. `'  .JMML. `Mbmmd' .JMMmmmmMMM .JMML.           */
// -- ****************************************************************/
// -- ****************************************************************/
/* -- ****************************************************************/
/* --                          droneX.go                             */
/* --                                                                */
/* --  Descripcion: Estructura general de los Drones.                */
/* --                                                                */
/* --   @ Autor  : Rodrigo G. Higuera M. <rodrigoghm@gmail.com>      */
/* --                                                                */
/* --  © 2022 - Mercado Libre - Desafio Tecnico SOLID                */
/* -- ****************************************************************/

package droneX

import "fmt"

func (x *DroneX) Volar() string {
	str := "[Volando] En el aire, Todo OK, Balanceo Automatico Habilitado ..."
	fmt.Printf(">>> Volando Drone[%s] | Modelo [%s]... \n", x.Nombre, x.Modelo)
	x.Preparar()
	x.Despegar()
	x.HealthCheck(str)
	return str
}

func (x *DroneX) Preparar() {
	x.CheckBateria()
	x.CheckHelices()
}

func (x *DroneX) CheckBateria() {
	fmt.Println("[Preparando] Chequeo de estado de bateria ... ")
}

func (x *DroneX) CheckHelices() {
	fmt.Println("[Preparando] Chequeo de Estado de las Helices ... ")
}

func (x *DroneX) Despegar() {
	fmt.Println("[Despengando] Despegando Ahora... ")
}

func (x *DroneX) HealthCheck(str string) {
	fmt.Println(str)
}

func (x *DroneX) SetNombre(n string) {
	x.Nombre = n
}

func (x *DroneX) SetModelo(m string) {
	x.Modelo = m
}

func (x *DroneX) GetNombre() string {
	return x.Nombre
}

func (x *DroneX) GetModelo() string {
	return x.Modelo
}
