// -- ****************************************************************/
// -- ****************************************************************/
// --                                                 ,,             */
// --          `7MMM.     ,MMF'         `7MMF'        db             */
// --            MMMb    dPMM             MM                         */
// --            M YM   ,M MM   .gP"Ya    MM        `7MM             */
// --            M  Mb  M' MM  ,M'   Yb   MM          MM             */
// --            M  YM.P'  MM  8M""""""   MM      ,   MM             */
// --            M  `YM'   MM  YM.    ,   MM     ,M   MM             */
// --          .JML. `'  .JMML. `Mbmmd' .JMMmmmmMMM .JMML.           */
// -- ****************************************************************/
// -- ****************************************************************/
/* -- ****************************************************************/
/* --                          droneY.go                             */
/* --                                                                */
/* --  Descripcion: Estructura general de los Drones.                */
/* --                                                                */
/* --   @ Autor  : Rodrigo G. Higuera M. <rodrigoghm@gmail.com>      */
/* --                                                                */
/* --  © 2022 - Mercado Libre - Desafio Tecnico SOLID                */
/* -- ****************************************************************/

package droneY

import "fmt"

func (y *DroneY) Volar() string {
	str := "[Volando] Estoy Dando Vueltassssss ..."
	fmt.Printf(">>> Volando Drone[%s] | Modelo [%s]... \n", y.DroneX.Nombre, y.DroneX.Modelo)
	y.Preparar()
	y.Despegar()
	y.HealthCheck()
	y.DarVueltas(str)
	return str
}

func (y *DroneY) Preparar() {
	y.CheckBateria()
	y.CheckHelices()
}

func (y *DroneY) CheckBateria() {
	fmt.Println("[Preparando] Chequeo de estado de bateria ... ")
}

func (y *DroneY) CheckHelices() {
	fmt.Println("[Preparando] Chequeo de Estado de las Helices ... ")
}

func (y *DroneY) Despegar() {
	fmt.Println("[Despengando] Despegando Ahora... ")
}

func (y *DroneY) HealthCheck() {
	fmt.Println("[Volando] En el aire, Todo OK, Balanceo Automatico Habilitado ... ")
}

func (y *DroneY) DarVueltas(str string) {
	fmt.Println(str)
}

func (y *DroneY) SetNombre(n string) {
	y.DroneX.Nombre = n
}

func (y *DroneY) SetModelo(m string) {
	y.DroneX.Modelo = m
}

func (y *DroneY) GetNombre() string {
	return y.DroneX.Nombre
}

func (y *DroneY) GetModelo() string {
	return y.DroneX.Modelo
}
